<?php
require ('../html/header.html'); 
require ('../html/top.html');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
$headers = "From: advaleraa@yahoo.com";

error_reporting(E_ALL);
require_once ("../../person/Person.php");

$firstName = $_POST['firstName'];
$lastName = $_POST['lastName'];
$birthDate = date("Y-m-d", strtotime($_POST['birthDate']));
$email = $_POST['email'];

$person = new Person($firstName, $lastName, $birthDate, $email);

$message = 'Hola, '.$person->fullName().', según su fecha de nacimiento, usted tiene '.$person->age();

echo $message;

$statusMail = mail($person->email,"Mensaje de Registro",$message,$headers);

if($statusMail == true){
	echo "La anterior información ha sido enviada a su correo electrónico: ".$person->email;
}else{
	echo "No hemos podido enviar la información. Disculpe las molestias.";
}
?>