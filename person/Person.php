<?php

/**
* 
*/
class Person
{

	public $firstName;
	public $lastName;
	public $birthDate;
	public $email;
   	
	function __construct($firstName, $lastName, $birthDate, $email)
	{
		$this->firstName = $firstName;
		$this->lastName = $lastName;
		$this->birthDate = $birthDate;
		$this->email = $email;
	}

	public function fullName()
	{
		return "{$this->firstName} {$this->lastName}";
	}

	public function age()
	{
		$message = "";
		$year = date('Y', strtotime($this->birthDate));
		$month = date('m', strtotime($this->birthDate));
		$day = date('d', strtotime($this->birthDate));
		if(date('m')>$month){
			$age=date('Y')-$year;
		}else if(date('m')<$month){
			$age=date('Y')-$year-1;
		}else{
			if(date('d')>$day){
				$age=date('Y')-$year;
				$pday= date('d')-$day;
				$message = '¡Cumpliste hace sólo: '.$pday.' días! ¡Feliz cumpleaños!<br>';
			}else if (date('d')==$day){
				$age=date('Y')-$year-1;
				$message = '¡Feliz, Feliz cumpleaños!<br>';
			}else{
				$age=date('Y')-$year-1;
				$mday= $day-date('d');
				$message = '¡Tan sólo faltan: '.$mday.' días para tu gran día!<br>';
			}
		}
		return $age." años. <br>".$message;
	}

}

?>